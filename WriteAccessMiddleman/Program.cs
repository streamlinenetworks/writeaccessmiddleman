﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WriteAccessMiddleman
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
              
                string[] files = Directory.GetFiles(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories);
                
                if (files.Length > 1)
                {
                    Console.WriteLine("Found {0} files to move.\n", files.Length-1);
                    
                    string[] dirs = Directory.GetDirectories(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories);

                    foreach (string dir in dirs)
                    {
                        string newPath = dir.Substring(14);
                        newPath = "C:\\test_read\\" + newPath;
                        if (!Directory.Exists(newPath))
                        {
                            Directory.CreateDirectory(newPath);
                            Console.WriteLine("Created directory {0}", newPath);
                        }
                    }
                    foreach (string file in files)
                    {
                        if (!file.EndsWith("WriteAccessMiddleman.exe"))
                        {
                            string newPath = file.Substring(14);
                            newPath = "C:\\test_read\\" + newPath;

                            try{
                                if (File.Exists(newPath))
                                {
                                    File.Delete(newPath);
                                }
                                
                                File.Move(file, newPath);
                                File.Delete(file);
                                Console.WriteLine("{0} was moved to {1}.", file, newPath);
                            }catch(Exception ex)
                            {
                                Console.WriteLine("File in use, cannot transfer. Skipping...");
                            }
                            
                        }
                    }
                    Console.WriteLine("\nSuccessfully Moved {0} Files.\n", files.Length - 1);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Scanned, no new files found.");
                }
                System.Threading.Thread.Sleep(30000);
            }
        }
    }
}
